from django.conf.urls import patterns, include, url
# -*- encoding: utf-8 -*-
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^mysite/', include('mysite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^$', 'strona.views.index',name='index'),
     url(r'^tabela$', 'strona.views.tabelkowanie'),
     url(r'^sciaganie_tabeli$', 'strona.views.tabelkowanie2'),
     url(r'^sklad_druzyny$', 'strona.views.sklad_druzyny',name='druzyny'),
     url(r'^sciaganie_druzyn$', 'strona.views.sciaganie_druzyn'),
     url(r'^usuwanie$', 'strona.views.usuwanie_baz'),
     url(r'^strzelcy$', 'strona.views.lista_strzelcow'),
     url(r'^zrob_tabele$', 'strona.views.zrob_tabele'),
     url(r'^statystyka$', 'strona.views.statystyka'),
     url(r'^statystyka/(.{1,50})$', 'strona.views.stat'),

)
