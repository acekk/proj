# Create your views here.
# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from models import tabela_glowna,druzyna,strzelcy,statystyka
import requests
import threading
from bs4 import BeautifulSoup



def index(request):
    t = loader.get_template('strona/index.html')
    c = Context({
    })
    return HttpResponse(t.render(c))

def tabelkowanie(request):
    tabela = tabela_glowna.objects.all()
    context = {'tabelka': tabela}
    return render(request, 'strona/tabela.html', context)

def tabelkowanie2(request):
    tabela_glowna.objects.all().delete()
    response = requests.get('http://www.ekstraklasa.org/index.php?typ=tabela&id=20')
    html = response.text
    parsed = BeautifulSoup(html)
    cialo_tabeli = parsed.find('table','table_big')
    wiersze = cialo_tabeli.find_all('tr')
    for i in range(2,len(wiersze)):
        komorka = wiersze[i].find_all('td')
        wpis = tabela_glowna(
            Lp=komorka[0].text,
            Nazwa_klubu=komorka[2].text,
            Ilosc_meczy = komorka[3].text,
            Ilosc_punktow =komorka[4].text,
            Ilosc_zwyciestw = komorka[5].text,
            Ilosc_przegranych = komorka[6].text,
            Ilosc_remisow = komorka[7].text,
            Bramki_zdobyte = komorka[8].text.split('-')[0],
            Bramki_stracone = komorka[8].text.split('-')[1]
        )
        wpis.save()
    baza = tabela_glowna.objects.all()
    context = {'tabelka': baza}
    return render(request, 'strona/tabela.html', context)


def sciaganie_druzyn(request):
    t = threading.Thread(target=sciaganie_druzyn2(request))
    t.setDaemon(True)
    t.start()
    #if t.isAlive():
        #return HttpResponseRedirect(reverse('index'))
    return HttpResponseRedirect(reverse('druzyny'))

def sciaganie_druzyn2(request):
    baza_linkow = []
    obroncy = ''
    bramkarze = ''
    pomocnicy = ''
    napastnicy = ''
    licznik = 0
    response = requests.get('http://ligapolska.info/pages/druzyny-ekstraklasy.php')
    html = response.text
    parsed = BeautifulSoup(html)
    tabelka = parsed.find('table',class_='tabela-srodek')
    linki = tabelka.find_all('a')
    for i in range(16):
        baza_linkow.append(linki[i]['href'])
    for i in baza_linkow:
        resp = requests.get(i)
        html = resp.text
        parsed = BeautifulSoup(html)
        tabela = parsed.find_all('table')[1]
        if len(tabela.find_all('tr'))==3:
            konkretny_wiersz = tabela.find_all('tr')[2]
        else:
            konkretny_wiersz = tabela.find_all('tr')[3]
        trener = konkretny_wiersz.find_all('p')[0].text.split(':')[1]
        ktos = konkretny_wiersz.find_all('p')[1]
        ktos2 = konkretny_wiersz.find_all('p')[2]
        ktos3 = konkretny_wiersz.find_all('p')[3]
        ktos4 = konkretny_wiersz.find_all('p')[4]
        for j in range(len(ktos.text.split(':')[1].split(','))):
            #bramkarze.append(ktos.text.split(':')[1].split(',')[j])
            bramkarze += ktos.text.split(':')[1].split(',')[j] + ' ; '
        for j in range(len(ktos2.text.split(':')[1].split(','))):
            #obroncy.append(ktos2.text.split(':')[1].split(',')[j])
            obroncy += ktos2.text.split(':')[1].split(',')[j]+ ' ; '
        for j in range(len(ktos3.text.split(':')[1].split(','))):
            #pomocnicy.append(ktos3.text.split(':')[1].split(',')[j])
            pomocnicy += ktos3.text.split(':')[1].split(',')[j]+ ' ; '
        for j in range(len(ktos4.text.split(':')[1].split(','))):
            #napastnicy.append(ktos4.text.split(':')[1].split(',')[j])
            napastnicy += ktos4.text.split(':')[1].split(',')[j]+ ' ; '
        team = druzyna(
            Lp = licznik,
            Nazwa_klubu = linki[licznik].text,
            Trener = trener,
            Bramkarze = bramkarze,
            Obroncy = obroncy,
            Pomocnicy = pomocnicy,
            Napastnicy = napastnicy
        )
        team.save()
        obroncy = ''
        bramkarze = ''
        pomocnicy = ''
        napastnicy = ''
        licznik+=1
    return HttpResponseRedirect(reverse('index'))

def sklad_druzyny(request):
    baza_druzyn = druzyna.objects.all()
    context = {'druzyny': baza_druzyn}
    return render(request, 'strona/sklad_druzyny.html', context)

def usuwanie_baz(request):
    tabela_glowna.objects.all().delete()
    druzyna.objects.all().delete()
    strzelcy.objects.all().delete()
    return HttpResponseRedirect(reverse('index'))

def lista_strzelcow(request):
    lista = {}
    #data = []
    response = requests.get('http://www.ekstraklasa.org/index.php?typ=klasyfikacja')
    html = response.text
    parsed = BeautifulSoup(html)
    div = parsed.find('div',id='trescNewsa')
    a = div.find_all('a')
    for i in range(len(a)-1):
        imie = a[i].find_all('p')[1].text
        gol = a[i].find_all('p')[2].text
        lista[imie]=gol
    baza = druzyna.objects.all()
    for i in lista:
        for j in baza:
            if j.Napastnicy.count(i)>0:
                #data.append(i +';'+ lista[i] +';'+ j.Nazwa_klubu +';'+ 'napastnik')
                czlek = strzelcy(Pilkarz=i,Nazwa_klubu=j.Nazwa_klubu
                ,Pozycja='Napastnik',Gole=lista[i])
                czlek.save()
            elif j.Obroncy.count(i)>0:
                # data.append(i +';'+ lista[i] +';'+ j.Nazwa_klubu +';' + 'obronca')
                czlek = strzelcy(Pilkarz=i,Nazwa_klubu=j.Nazwa_klubu
                ,Pozycja='Obronca',Gole=lista[i])
                czlek.save()
            elif j.Pomocnicy.count(i)>0:
                #data.append(i +';'+ lista[i] +';'+ j.Nazwa_klubu +';'+ 'pomocnik')
                czlek = strzelcy(Pilkarz=i,Nazwa_klubu=j.Nazwa_klubu
                ,Pozycja='Pomocnik',Gole=lista[i])
                czlek.save()
    baza2 = strzelcy.objects.all()
    context = {'baza_strzelcow':baza2,'baza_druzyn':baza}
    return render(request, 'strona/strzelcy.html', context)


def zrob_tabele(request):
    t = threading.Thread(target=sciaganie_druzyn2(request))
    t.setDaemon(True)
    t.start()
    if t.isAlive():
        s = threading.Thread(target=lista_strzelcow(request))
        s.setDaemon(True)
        s.start()
    k = threading.Thread(target=tabelkowanie2(request))
    k.setDaemon(True)
    k.start()
    return HttpResponseRedirect(reverse('index'))

def statystyka(request):
    baza = druzyna.objects.all()
    teamy = []
    for i in baza:
        teamy.append(i.Nazwa_klubu)
    context = {'baza':teamy}
    return render(request, 'strona/statystyka.html', context)

def stat(request,cos):
    baza = druzyna.objects.all()
    tab = tabela_glowna.objects.all()
    dane_ogolne = []
    data = []
    for i in baza:
        if i.Nazwa_klubu==cos.split('/')[0]:
            dane_ogolne.append(i.Trener)
    for i in tab:
        if i.Nazwa_klubu==cos.split('/')[0]:
            gol_na_mecz = float(i.Bramki_zdobyte)/30
            data.append(gol_na_mecz)
    context = {'dane':dane_ogolne,'dane_s:':data}
    return render(request,'strona/stat.html',context)