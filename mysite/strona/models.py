from django.db import models

# Create your models here.
class tabela_glowna(models.Model):
    Lp = models.TextField(max_length=3)
    Nazwa_klubu = models.CharField(max_length=50)
    Ilosc_meczy = models.CharField(max_length=5)
    Ilosc_punktow = models.CharField(max_length=5)
    Ilosc_zwyciestw = models.CharField(max_length=5)
    Ilosc_przegranych = models.CharField(max_length=5)
    Ilosc_remisow = models.CharField(max_length=5)
    Bramki_zdobyte = models.CharField(max_length=5)
    Bramki_stracone = models.CharField(max_length=5)


class druzyna(models.Model):
    Lp = models.TextField(max_length=3)
    Nazwa_klubu = models.CharField(max_length=50)
    Trener = models.CharField(max_length=50)
    Bramkarze = models.CharField(max_length=200)
    Obroncy = models.CharField(max_length=200)
    Pomocnicy = models.CharField(max_length=200)
    Napastnicy = models.CharField(max_length=200)

class strzelcy(models.Model):
    Pilkarz = models.CharField(max_length=50)
    Nazwa_klubu = models.CharField(max_length=50)
    Pozycja = models.CharField(max_length=50)
    Gole = models.CharField(max_length=5)

class statystyka(models.Model):
    Gole_na_mecz = models.CharField(max_length=10)
    Strata_na_mecz = models.CharField(max_length=10)